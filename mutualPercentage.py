#!/usr/bin/python3
from mastodon import Mastodon

from getpass import getpass
import os

instance = input("Your instance (eg: mastodon.social):")
api_base_url = f"https://{instance}"
username = "{u}@{i}".format(u=input("Your username (without the instance part):"), i=instance)

clientcredfilename  = f"pytooter_clientcred.{username}.secret"
usercredfilename    = f"pytooter_usercred.{username}.secret"

# Register app - only once!
if(not os.path.exists(clientcredfilename)):
    Mastodon.create_app(
         'mutualsApp',
         api_base_url = api_base_url,
         to_file = clientcredfilename
    )

# Log in to your account, once
if(not os.path.exists(usercredfilename)):
    mastodon = Mastodon(
        client_id = clientcredfilename,
        api_base_url = api_base_url
    )
    mastodon.log_in(
        input(f"E-mail used to create your account ({username}): "),
        getpass(f"Password for {username}: "),
        to_file = usercredfilename
    )


# Create actual API instance
mastodon = Mastodon(
    client_id = clientcredfilename,
    access_token = usercredfilename,
    api_base_url = api_base_url
)

print("fetching your account...")
me = mastodon.account_search(username)[0]
print("fetching your followers")
myFollows = mastodon.fetch_remaining(mastodon.account_following(me["id"]))
myMutuals = []
for follow in myFollows:
    if ((mastodon.account_relationships(follow))[0]["followed_by"]):
        myMutuals.append(follow)
        print(f"\rveryfing if {follow['username']} follows you back... Yes.", end="\033[0K")
    else:
        print(f"\rveryfing if {follow['username']} follows you back... No.\033[0K")

print("Percentage of mutuals among your follows : " + str(int(len(myMutuals)/len(myFollows)*100)) + "%")
